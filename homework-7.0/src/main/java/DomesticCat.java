public class DomesticCat extends Pet {
    public DomesticCat(String species, String nickname, int age) {
        super(species, nickname, age);
    }

    @Override
    void describe() {
        System.out.println("Hi,l am DomesticCat.");
    }

    @Override
    void respond() {
        System.out.println("DomesticCat is here.");
    }

    @Override
    void foul() {
        System.out.println("I havent make foul.");
    }
}
