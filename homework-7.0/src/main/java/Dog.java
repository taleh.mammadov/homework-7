public class Dog extends Pet{
    public Dog(String species, String nickname, int age) {
        super(species, nickname, age);
    }

    @Override
    void describe() {
        System.out.println("Hi,l am dog.");
    }

    @Override
    void respond() {
        System.out.println("Dog is here.");
    }

    @Override
    void foul() {
        System.out.println("I havent make foul.");
    }
}
