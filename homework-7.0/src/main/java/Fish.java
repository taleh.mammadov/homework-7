public class Fish extends Pet{
    public Fish(String species, String nickname, int age) {
        super(species, nickname, age);
    }
    @Override
    void describe() {
        System.out.println("Hi,l am Fish.");
    }

    @Override
    void respond() {
        System.out.println("Fish is here.");
    }

    @Override
    void foul() {
        System.out.println("I havent make foul.");
    }
}
