
public class homework7 {
    public static void main(String[] args) {
        for(Pet.species species: Pet.species.values()){
            System.out.println("Specie: "+species.name());
        }
        Pet fish = new Fish("Farel","Clev",12);
        fish.eat();
        fish.describe();
        fish.foul();
        fish.respond();
        Pet roboCat = new RoboCat("Robo","Miao",3);
        roboCat.eat();
        roboCat.describe();
        roboCat.foul();
        roboCat.respond();
        Pet domesticCat = new DomesticCat("PetCat","Milano",4);
        domesticCat.eat();
        domesticCat.describe();
        domesticCat.foul();
        domesticCat.respond();
        Pet dog = new Dog("Qanqal","Boss",5);
        dog.eat();
        dog.describe();
        dog.foul();
        dog.respond();

        Man man = new Man();
        man.greetPet();
        man.repairCar();
        Woman woman = new Woman();
        woman.greetPet();
        woman.makeUp();



    }
}
