public abstract class Pet {
    private String species;
    private String nickname;
    private int age;

    public Pet(String species, String nickname, int age) {
        this.species = species;
        this.nickname = nickname;
        this.age = age;
    }
    enum species{
        UNKNOWN,DOG,CAT,BIRD;
    }
    public void  eat(){
        System.out.println("Take your meal..");
    }
    abstract void describe();
    abstract void respond();
    abstract void foul();

    public String getSpecies() {
        return species;
    }

    public void setSpecies(String species) {
        this.species = species;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    @Override
    public String toString() {
        return "Pet{" +
                "species='" + species + '\'' +
                ", nickname='" + nickname + '\'' +
                ", age=" + age +
                '}';
    }
}
