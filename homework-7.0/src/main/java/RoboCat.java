public class RoboCat extends Pet{
    public RoboCat(String species, String nickname, int age) {
        super(species, nickname, age);
    }

    @Override
    void describe() {
        System.out.println("Hi,l am RoboCat.");
    }

    @Override
    void respond() {
        System.out.println("RoboCat is here.");
    }

    @Override
    void foul() {
        System.out.println("I havent make foul.");
    }
}
